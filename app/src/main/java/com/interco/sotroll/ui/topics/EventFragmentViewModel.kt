package com.interco.sotroll.ui.topics

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.interco.sotroll.R
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.database.entity.CommentsData
import com.interco.sotroll.database.entity.TopicsData
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import io.reactivex.Completable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

class EventFragmentViewModel(application: Application) : BaseActivityViewModel(application) {

    internal var topiclist: MutableLiveData<ArrayList<Topic>> = MutableLiveData()
    private var topiclistError: MutableLiveData<String>? = null
    var isOnline: MutableLiveData<Boolean> = MutableLiveData()
    var currentUser: UserModel = Gson().fromJson(preferencesHelper.getString("currentUser"), UserModel::class.java)

    init {

        isOnline.value = null
        getAllTopics()
    }


    private fun getAllTopics() {
        subscribeMainThred(networkApiService.getAllTopics(), Consumer { result: List<Topic> ->
            toggleLoading.postValue(true)
            topiclist.postValue(result as ArrayList<Topic>)
            toggleLoading.postValue(false)
            addAllTopicsToDb(topicToListTopic(result))

            isOnline.postValue(true)

        }, Consumer {
            toggleLoading.postValue(false)
            if (it.message == getString(R.string.error_no_network) || it.message!!.contains("failed")) {
                isOnline.postValue(false)
            }

            Timber.e("ERROR  FINDING TOPICS IN COULD  %s", it.message)
            getDbTopics()

        })
    }


    private fun getDbTopics() {
        subscribeMainThred(dataBaseHelper.TopicsDataBase().getTopics(), Consumer { result: List<TopicsData> ->
            toggleLoading.postValue(true)
            topiclist.postValue(topicListToTopic(result))
            toggleLoading.postValue(false)

        }, Consumer {
            toggleLoading.postValue(true)
            topiclistError?.postValue(it.message)
            toggleLoading.postValue(false)

        })
    }


    @SuppressLint("CheckResult")
    private fun addAllTopicsToDb(topiclist: List<TopicsData>) {
        Completable.fromAction {
            dataBaseHelper.TopicsDataBase().deleteAll()
        }.subscribeOn(Schedulers.io())
                .subscribe {
                    Timber.e("db cleared ;)")
                    Completable.fromAction {
                        dataBaseHelper.TopicsDataBase().insertMultipleTopics(topiclist)
                    }.subscribeOn(Schedulers.io())
                            .subscribe {
                                Timber.e(" topick added to DB ! Yes Bro ")
                            }
                }


    }

    fun removeElement(id: Long) {
        toggleLoading.postValue(true)
        subscribeMainThred(networkApiService.removeTopic(id), Consumer {
            toggleLoading.postValue(false)
            Timber.e("deleted !")
        }, Consumer {
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                Timber.e("ERROR Removing TOPIC {${it.message}}")
            }
        })
    }

    fun likeTopic(id: Long) {
        subscribeMainThred(networkApiService.likeTopic(id, currentUser.id!!), Consumer {
        }, Consumer {
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                Timber.e("ERROR ADD COMMENT {${it.message}}")
            }

        })
    }

    fun unLikeTopic(id: Long) {
        subscribeMainThred(networkApiService.unlikeTopic(id, currentUser.id!!), Consumer {
        }, Consumer {
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                Timber.e("ERROR ADD COMMENT {${it.message}}")
            }
        })
    }

    fun postComment(topicId: Long, commentContent: String) {
        subscribeMainThred(networkApiService.addComment(topicId, commentContent, currentUser.id!!), Consumer {
        }, Consumer {
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                Timber.e("ERROR ADD COMMENT {${it.message}}")
            }

        })
    }

    fun updateComment(idComment: Long, commentContent: String) {
        subscribeMainThred(networkApiService.updateComment(idComment, commentContent), Consumer {
            getAllTopics()
        }, Consumer {
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {

                Timber.e("ERROR ADD COMMENT {${it.message}}")
            }
        })
    }

    fun deleteComment(comment: Comments) {

        Timber.e(" you gonna remove  $comment")

        subscribeCompletable(networkApiService.deleteComment(comment.id!!), Action {
            getAllTopics()
        }, Consumer {
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                getAllTopics()
                Timber.e("ERROR REMOVE COMMENT {$it}")
            }
        })


    }


    //************** mappers *****************


    private fun topicListToTopic(topicDataList: List<TopicsData>): ArrayList<Topic> {

        val topicList: MutableList<Topic> = ArrayList()

        for (elm in topicDataList) {
            topicList.add(Topic(elm.topicsId, elm.creator, elm.imageUrl, elm.content,
                    elm.location, elm.date_creation, elm.date_RDV, elm.likes, commentData2Comment(elm.comments)))

        }
        return topicList as ArrayList<Topic>
    }

    private fun topicToListTopic(topicDataList: List<Topic>): ArrayList<TopicsData> {

        val topicList: MutableList<TopicsData> = ArrayList()

        for (elm in topicDataList) {
            topicList.add(TopicsData(elm.id!!, elm.owner
                    ?: UserModel(), elm.picture, elm.content.toString(),
                    elm.location.toString(), elm.createdDate
                    ?: Calendar.getInstance().time, elm.dateRdv, elm.nbrLikes, comment2CommentData(elm.commentList)))
        }
        return topicList as ArrayList<TopicsData>
    }

    private fun commentData2Comment(list: MutableList<CommentsData>): MutableList<Comments> {
        val topicListCommnt: MutableList<Comments> = ArrayList()
        for (element in list) {
            topicListCommnt.add(Comments(element.id, element.user, element.content, element.createdDate))
        }
        return topicListCommnt

    }

    private fun comment2CommentData(list: MutableList<Comments>): MutableList<CommentsData> {
        val topicListCommnt: MutableList<CommentsData> = ArrayList()
        for (element in list) {
            topicListCommnt.add(CommentsData(element.id, element.user, element.content, element.createdDate))
        }
        return topicListCommnt

    }

}
