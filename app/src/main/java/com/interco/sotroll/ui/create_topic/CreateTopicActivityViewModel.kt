package com.interco.sotroll.ui.create_topic

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import io.reactivex.functions.Consumer
import java.util.*


class CreateTopicActivityViewModel(application: Application) : BaseActivityViewModel(application) {

    var currentUser: UserModel = Gson().fromJson(preferencesHelper.getString("currentUser"), UserModel::class.java)

    val topicposted: MutableLiveData<Boolean> = MutableLiveData()

    init {
        topicposted.value = false
    }

    @SuppressLint("CheckResult")
    fun postTopic(content: String, locationText: String?, image: String?, dateRdv: Date) {
        val topic = Topic()

        with(topic) {
            this.content = content
            this.picture = image ?: "no-image"
            this.owner = currentUser
            this.dateRdv = dateRdv
            this.location = if (locationText.isNullOrEmpty()) " lieux : indéfinie " else locationText

        }

        subscribeMainThred(networkApiService.addTopic(topic, currentUser.id!!), Consumer {
            if (it != null)
                topicposted.postValue(true)
        }, Consumer {
            topicposted.postValue(false)

        })


    }
}
