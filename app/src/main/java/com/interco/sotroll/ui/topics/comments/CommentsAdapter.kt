package com.interco.sotroll.ui.topics.comments

import android.content.Context
import android.view.ViewGroup
import com.interco.sotroll.R
import com.interco.sotroll.base.adapter.GenericRecyclerAdapter
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.ui.topics.listner.TopicListener

/**
 * Created by emine on 12/02/2019.
 */
class CommentsAdapter(context: Context, var topicListener: TopicListener<*>, var  topic: Topic,  var topicpositon : Int, var currentUSer : UserModel) : GenericRecyclerAdapter<Comments
        , TopicListener<Comments>, CommentsViewHolder>(context) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        return CommentsViewHolder(inflate(R.layout.item_comment, parent),topicListener, topic, topicpositon,currentUSer)
    }

}
