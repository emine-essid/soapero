package com.interco.sotroll.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.interco.sotroll.R;
import com.interco.sotroll.base.activity.BaseActivity;
import com.interco.sotroll.ui.home.HomeActivity;
import com.interco.sotroll.utils.ConnectivityInterceptor;
import com.interco.sotroll.utils.DialogUtils;

import java.util.Objects;

import timber.log.Timber;

public class SplashActivity extends BaseActivity<SplashViewModel> implements View.OnClickListener {

    private static final int RC_SIGN_IN = 9999;
    public GoogleSignInAccount account;
    private GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initviews();


        //addFragment(getSupportFragmentManager(), new BlankFragment(), R.commentId.fragmentContainer, true);
    }

//    private void initBinding() {
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_about);
//        binding.setViewModel(getViewModel());
//    }

    private void initviews() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        SignInButton signInButton = findViewById(R.id.sign_in_button_gmail);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);

        handleObservables();
    }

    private void handleObservables() {


        Objects.requireNonNull(getViewModel()).getShowError().observe(this, error -> {
            if (!error.isEmpty()) {
                toggleLoading(false);
                DialogUtils.INSTANCE.showBottomMessage(this, error, true);
                mGoogleSignInClient.signOut();
            }
        });


        getViewModel().getCanPass().observe(this, accountEmail -> {
            if (!accountEmail.isEmpty()) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                intent.putExtra("email", accountEmail);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Objects.requireNonNull(getViewModel()).getCurrentUser().observe(this, userModel -> {
            if (userModel != null) {
                getViewModel().postuser(userModel);
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                intent.putExtra("email", userModel.getEmail());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);
            if (getViewModel() != null)
                getViewModel().shouldIDPass(account);
            else
                Timber.e("PRESENTER NULLL !!!!!");
            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Timber.e("signInResult:failed code %s", e.getStatusCode());
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sign_in_button_gmail) {

            if (!ConnectivityInterceptor.Companion.isConnectedToNetwork(this)) {
                DialogUtils.INSTANCE.showBottomMessage(this, getString(R.string.error_no_network), true);
            } else {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }

        }
    }


}
