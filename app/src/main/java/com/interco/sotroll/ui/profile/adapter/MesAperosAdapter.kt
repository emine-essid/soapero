package com.interco.sotroll.ui.profile.adapter

import android.content.Context
import android.view.ViewGroup
import com.interco.sotroll.R
import com.interco.sotroll.base.adapter.GenericRecyclerAdapter
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel


/**
 * Created by emine on 12/02/2019.
 */
class MesAperosAdapter(context: Context, val currentUser: UserModel) : GenericRecyclerAdapter<Topic, MesAperosListener<Topic>, MyAperosViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAperosViewHolder {

        return MyAperosViewHolder(inflate(R.layout.cell, parent), currentUser)

    }


}
