package com.interco.sotroll.ui.topics.adapter.topics

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.button.MaterialButton
import com.interco.sotroll.R
import com.interco.sotroll.base.viewholder.BaseViewHolder
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.ui.topics.comments.CommentsAdapter
import com.interco.sotroll.ui.topics.listner.TopicListener
import com.interco.sotroll.utils.ConnectivityInterceptor
import com.interco.sotroll.utils.ImageUtil
import timber.log.Timber
import java.text.SimpleDateFormat


/**
 * Created by emine on 12/02/2019.
 */
open class NewTopicAdapterViewHolder(itemView: View, val currentUser: UserModel) : BaseViewHolder<Topic, TopicListener<Topic>>(itemView) {


    private val commentsRecyclerView = itemView.findViewById<RecyclerView>(R.id.topic_comments)
    private val topicUserName = itemView.findViewById<TextView>(R.id.topic_user_name)
    private val topicUserPhoto = itemView.findViewById<ImageView>(R.id.topic_user_image)
    private val topicContentTewt = itemView.findViewById<TextView>(R.id.topic_content_text)
    private val topicContentImage = itemView.findViewById<ImageView>(R.id.topic_content_image)
    private val topicContentInfos = itemView.findViewById<TextView>(R.id.topic_infos)
    private val topicLikesImage = itemView.findViewById<ImageView>(R.id.topic_likes_image)
    private val topicLikesText = itemView.findViewById<TextView>(R.id.topic_likes_text)
    private val topicCreationDate = itemView.findViewById<TextView>(R.id.topicCreationDate)
    private val likeContainer = itemView.findViewById<LinearLayout>(R.id.liked_container)
    private val addAommentContainer = itemView.findViewById<LinearLayout>(R.id.add_comment_container)
    private val maincard = itemView.findViewById<LinearLayout>(R.id.maincard)
    private val removEvent = itemView.findViewById<ImageView>(R.id.remoce_event)

    private val addCommentEditText = itemView.findViewById<EditText>(R.id.add_comment_edit_text)
    private val materialButton = itemView.findViewById<MaterialButton>(R.id.submit_comment_btn)


    init {
        setFadeAnimation(itemView, 1000)
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat", "CheckResult")
    override fun onBind(item: Topic, listener: TopicListener<Topic>?, index: Int
    ) {

        val manager = LinearLayoutManager(itemView.context)
        commentsRecyclerView.layoutManager = manager
        commentsRecyclerView.isNestedScrollingEnabled = true

        Timber.e("----- Item commentList %s", item.commentList.size)

        if (!item.commentList.isEmpty()) {
            val adapter = CommentsAdapter(itemView.context, listener!!, item, index, currentUser)
            adapter.clear()
            adapter.setItems(item.commentList)
            commentsRecyclerView.adapter = adapter
        } else {
            commentsRecyclerView.visibility = View.GONE
        }

        if (item.picture == null || item.picture!!.isEmpty()) {
            topicContentImage.visibility = View.GONE
        } else if (!item.picture!!.contains("no-image")) {
            Glide.with(itemView.context)
                    .load(ImageUtil.convertbase64Str(item.picture))
                    .into(topicContentImage)
        } else {
            topicContentImage.visibility = View.GONE
        }


        if (item.owner!!.photo == null || item.owner!!.photo!!.isEmpty()) {
            topicUserPhoto.setBackgroundResource(R.drawable.geek_avatar)
        } else {

            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.geek_avatar)
            requestOptions.error(R.drawable.geek_avatar)

            Glide.with(itemView.context)
                    .load(item.owner!!.photo)
                    .apply(requestOptions)
                    .apply(RequestOptions.circleCropTransform())
                    .into(topicUserPhoto)
        }


        topicUserName.text = "${item.owner?.nom}"
        topicContentTewt.text = "${item.content}"
        val df = SimpleDateFormat("MM/dd/yyyy - HH:mm:ss")
        val dayOnly = SimpleDateFormat("MM/dd/yyyy")
        topicContentInfos.text = "${item.location}\n${df.format(item.dateRdv)}"
        topicCreationDate.text = dayOnly.format(item.createdDate)



        if (!item.nbrLikes.contains(currentUser)) {
            topicLikesImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.love_disabled))
            makeItbounce(itemView.context, likeContainer)
            if (item.nbrLikes.isEmpty()) {
                topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
                topicLikesText.text = itemView.context.getString(R.string.soit_lepremier_a_participe)
            } else {

                if (item.nbrLikes.size > 1)
                    topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
                else
                    topicLikesText.text = "${item.nbrLikes.size} personne y participe !"

            }
        } else {
            if (item.nbrLikes.isEmpty()) {
                topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
                topicLikesText.text = itemView.context.getString(R.string.soit_lepremier_a_participe)
            } else {
                when {
                    item.nbrLikes.size == 1 -> topicLikesText.text = itemView.context.getString(R.string.tu_y_participe)
                    item.nbrLikes.size > 1 -> topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
                    else -> topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
                }
            }
            topicLikesImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.love_enabled))
            makeItbounce(itemView.context, likeContainer)

        }

        likeContainer.setOnClickListener {
            if (ConnectivityInterceptor.isConnectedToNetwork(itemView.context)) {
                listener?.noConnection(true)
                listener?.topicLiked(item, index, topicLikesImage, item.nbrLikes.contains(currentUser))

            } else
                listener?.noConnection(false)

        }

        maincard.setOnClickListener {
            if (ConnectivityInterceptor.isConnectedToNetwork(itemView.context)) {
                listener?.noConnection(true)
                if (addAommentContainer.visibility == View.GONE) {
                    addAommentContainer.visibility = View.VISIBLE
                } else {
                    addAommentContainer.visibility = View.GONE
                }
            } else {
                listener?.noConnection(false)

            }

        }

        addCommentEditText.setHorizontallyScrolling(false)
        addCommentEditText.maxLines = 4
        materialButton.setOnClickListener {
            if (ConnectivityInterceptor.isConnectedToNetwork(itemView.context)) {
                listener?.noConnection(true)
                if (!addCommentEditText.text.toString().isEmpty()) {
                    listener?.postComment(item, index, addCommentEditText.text.toString())
                    addCommentEditText.text.clear()
                }
                addAommentContainer.visibility = View.GONE
            } else {
                listener?.noConnection(false)

            }
        }



        if (item.owner!!.email == currentUser.email && ConnectivityInterceptor.isConnectedToNetwork(itemView.context)) {
            listener?.noConnection(true)
            removEvent.visibility = View.VISIBLE
            removEvent.setOnClickListener { listener?.removeEvent(item) }


        }
    }

    private fun makeItbounce(context: Context, view: View) {
        val myAnim = AnimationUtils.loadAnimation(context, R.anim.appair_anim_50)
        view.startAnimation(myAnim)

    }


    private fun setFadeAnimation(view: View, speed: Long) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = speed
        view.startAnimation(anim)
    }

}
