package com.interco.sotroll.ui.create_topic

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.transition.Explode
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.interco.sotroll.base.activity.BaseActivity
import com.interco.sotroll.utils.ImageUtil
import kotlinx.android.synthetic.main.activity_create_topic.*
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


class CreateTopicActivity : BaseActivity<CreateTopicActivityViewModel>() {

    private var b64SelectedImage: String? = null
    private var dateRdv: Date? = null
    private lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initTransitions()
        initViews()
        initObserver()

        context = this

        topic_content_text.apply {
            setHorizontallyScrolling(false)
            maxLines = 12
        }


    }

    private fun initTransitions() {
        window.apply {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
            enterTransition = Explode()
            exitTransition = Explode()
            enterTransition.excludeTarget(add_topic_btn_purple, true)
        }
        setContentView(com.interco.sotroll.R.layout.activity_create_topic)

    }

    private fun initObserver() {
        viewModel?.topicposted?.observe(this, Observer {
            if (it) {
                val returnIntent = Intent()
                //returnIntent.putExtra("result", result)
                setResult(Activity.RESULT_OK, returnIntent)
                finishAfterTransition()
            }
        })
    }

    private fun initViews() {

        topic_user_name.text = viewModel?.currentUser?.nom ?: "unnamed !"

        Glide.with(this)
                .load(viewModel?.currentUser?.photo)
                .apply(RequestOptions.circleCropTransform())
                .into(topic_user_image)


        content_image.setOnClickListener {
            requestAppPermissions()
        }

        add_topic_btn_purple.setOnClickListener {

            viewModel!!.postTopic(topic_content_text.text.toString(), topic_location_text.text.toString(), b64SelectedImage, dateRdv
                    ?: Date())

        }

        topic_infos.setOnClickListener {
            setupDate(it)
        }


    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    private fun setupDate(view: View) {
        val myCalendar: Calendar = Calendar.getInstance()

        val hour = myCalendar.get(Calendar.HOUR_OF_DAY)
        val minute = myCalendar.get(Calendar.MINUTE)

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

            myCalendar.apply {
                set(Calendar.YEAR, year)
                set(Calendar.MONTH, monthOfYear)
                set(Calendar.DAY_OF_MONTH, dayOfMonth)

            }.run {

                TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                    run {
                        myCalendar.set(Calendar.HOUR_OF_DAY, selectedHour)
                        myCalendar.set(Calendar.MINUTE, selectedMinute)
                    }.run {
                        dateRdv = myCalendar.time
                        val df = SimpleDateFormat("dd/MM/yyyy HH:mm")
                        (view as TextView).text = "le ${df.format(myCalendar.time)}"
                    }
                }, hour, minute, true).apply {
                    setTitle("Select Time of Apéro")
                    show()
                }


            }

        }
        DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show()


    }


    private val REQUEST_WRITE_STORAGE_REQUEST_CODE: Int = 99

    private fun requestAppPermissions() {


        if (hasReadPermissions() && hasWritePermissions()) {
            val intent = Intent()
            intent.apply {
                type = "image/*"
                action = Intent.ACTION_GET_CONTENT
            }
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 200)


        }

        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_WRITE_STORAGE_REQUEST_CODE) // your request code
    }

    private fun hasReadPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(baseContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun hasWritePermissions(): Boolean {
        return ContextCompat.checkSelfPermission(baseContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return
            }
            //val inputStream = this.contentResolver.openInputStream(data.data)
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data.data).apply {
                compress(Bitmap.CompressFormat.PNG, 50, ByteArrayOutputStream())
            }
            b64SelectedImage = null


            b64SelectedImage = ImageUtil.bitMapToString(bitmap)

            Glide.with(this)
                    .load(bitmap)
                    .into(content_image)

        }
    }


}
