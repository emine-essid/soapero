package com.interco.sotroll.ui.profile

import android.app.Application
import com.google.gson.Gson
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.models.UserModel

class ProfileFragmentViewModel(application: Application) : BaseActivityViewModel(application) {
    var currentUser: UserModel = Gson().fromJson(preferencesHelper.getString("currentUser"), UserModel::class.java)



}
