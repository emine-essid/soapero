package com.interco.sotroll.ui.topics.adapter.topics

import android.content.Context
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.interco.sotroll.R
import com.interco.sotroll.base.adapter.GenericRecyclerAdapter
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.ui.topics.listner.TopicListener


/**
 * Created by emine on 12/02/2019.
 */
class NewTopicAdapter(context: Context, val currentUser: UserModel) : GenericRecyclerAdapter<Topic, TopicListener<Topic>, NewTopicAdapterViewHolder>(context), Filterable {


    private val filteredData: MutableList<Topic> = ArrayList()

    fun setData(data: List<Topic>) {
        clear()
        filteredData.clear()
        addAll(data)
        filteredData.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return filteredData.size

    }

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                //setItems(filteredData)
                filteredData.clear()

                if (charString.isEmpty()) {
                    filteredData.addAll(getItems()!!)
                } else {
                    val filteredList = ArrayList<Topic>()
                    for (querry in getItems()!!) {
                        val topicContent = querry.content
                        val topicName = querry.owner!!.nom

                        if (topicName!!.toLowerCase().contains(charString.toLowerCase()) ||
                                topicContent!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(querry)
                        }
                    }
                    filteredData.addAll(filteredList)
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = filteredData
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                getItemViewType(0)
                notifyDataSetChanged()
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewTopicAdapterViewHolder {

        return NewTopicAdapterViewHolder(inflate(R.layout.item_event, parent), currentUser)

    }


}
