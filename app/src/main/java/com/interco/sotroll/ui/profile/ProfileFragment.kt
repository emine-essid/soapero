package com.interco.sotroll.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import com.interco.sotroll.R
import com.interco.sotroll.base.fragment.BaseFragment
import com.interco.sotroll.ui.profile.mes_aperos.MesAperosFragment
import com.interco.sotroll.ui.topics.EventsFragment
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : BaseFragment<ProfileFragmentViewModel>() {


    private lateinit var createdAperoText: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createdAperoText = view.findViewById(R.id.created_apero_nuber_profile)

        user_name_profile_small.text = viewModel!!.currentUser.nom
        user_name_profile.text = viewModel!!.currentUser.nom

        val photoProfile = view.findViewById<ImageView>(R.id.user_image_profile)
        val photoProfileSmall = view.findViewById<ImageView>(R.id.user_image_profile_small)

        Glide.with(this)
                .load(viewModel!!.currentUser.photo)
                .apply(RequestOptions.circleCropTransform())
                .into(photoProfile)

        Glide.with(this)
                .load(viewModel!!.currentUser.photo)
                .apply(RequestOptions.circleCropTransform())
                .into(photoProfileSmall)



        addFragment(MesAperosFragment(), R.id.aperos_profile_container, true)


        created_apero_btn.isActivated = true

        created_apero_btn.setOnClickListener {
            created_apero_btn.isActivated = true
            subscribed_apero_btn.isActivated = false
            showCreatedAperos()

        }

        subscribed_apero_btn.setOnClickListener {
            subscribed_apero_btn.isActivated = true
            created_apero_btn.isActivated = false
            showSubscribedAperos()

        }


        appBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->

            if (Math.abs(verticalOffset) - appBarLayout.totalScrollRange == 0) {
                //  Collapsed
                toolbar.visibility = View.VISIBLE
            } else {
                //Expanded
                toolbar.visibility = View.INVISIBLE
            }
        })


    }


    fun updateCreatedAperos(it: String) {
        createdAperoText.text = it
    }


    private fun showSubscribedAperos() {
        replaceFragmentNoAddToStack(EventsFragment(), R.id.aperos_profile_container, true)
    }

    private fun showCreatedAperos() {
        replaceFragmentNoAddToStack(MesAperosFragment(), R.id.aperos_profile_container, true)

    }
}

