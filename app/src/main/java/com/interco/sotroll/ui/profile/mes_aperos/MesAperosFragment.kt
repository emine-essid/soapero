package com.interco.sotroll.ui.profile.mes_aperos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.interco.sotroll.R
import com.interco.sotroll.base.eventBus.OfflineModeEvent
import com.interco.sotroll.base.eventBus.RxBus
import com.interco.sotroll.base.fragment.BaseFragment
import com.interco.sotroll.models.Topic
import com.interco.sotroll.ui.profile.ProfileFragment
import com.interco.sotroll.ui.profile.adapter.MesAperosAdapter
import com.interco.sotroll.ui.profile.adapter.MesAperosListener
import com.interco.sotroll.utils.DialogUtils




class MesAperosFragment : BaseFragment<MesAperosViewModel>() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mes_aperos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val myRecycler = view.findViewById<RecyclerView>(R.id.mes_aperos_list)
        val manager = LinearLayoutManager(getActivity())
        myRecycler.layoutManager = manager
        myRecycler.isNestedScrollingEnabled = true

        val adapter = MesAperosAdapter(getActivity()!!.baseContext, viewModel!!.currentUser)


        adapter.setListener(object : MesAperosListener<Topic> {
            override fun noConnection(haveIconnection: Boolean) {
            }

            override fun removeEvent(item: Topic) {
                DialogUtils.showActionDialog(getActivity()!!, " Voulez-vous vraiment supprimer cet éventement?", "Yes", "No", Runnable {
                    viewModel?.removeApero(item.id!!)

                })
            }
        })

        viewModel!!.topiclist.observe(this, Observer {
            if (it != null && !it.isEmpty()) {
                (parentFragment as ProfileFragment).updateCreatedAperos("Apéro Created ${it.size}")
                adapter.clear()
                adapter.setItems(it)
                myRecycler.adapter = adapter
            }
        })

        viewModel!!.isOnline.observe(this, Observer { isOnline ->
            if (isOnline != null) {
                if (isOnline) RxBus.getInstance().send(OfflineModeEvent("Event_is_online", true)) else RxBus.getInstance().send(OfflineModeEvent("Event_is_online", false))
            }
        })


    }


}

