package com.interco.sotroll.ui.topics.comments

import android.app.Activity
import android.content.Context
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.interco.sotroll.R
import com.interco.sotroll.base.viewholder.BaseViewHolder
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.ui.topics.listner.TopicListener
import com.interco.sotroll.utils.KeybordUtils

/**
 * Created by emine on 12/02/2019.
 */
open class CommentsViewHolder(itemView: View, var topicListener: TopicListener<*>, var topic: Topic, var topicpositon: Int, var currentUSer: UserModel) : BaseViewHolder<Comments, TopicListener<Comments>>(itemView) {


    private val commentUserName = itemView.findViewById<TextView>(R.id.topic_comment_username)
    private val commentContent: TextView = itemView.findViewById(R.id.topic_comment_content)
    private val commentContentEditText = itemView.findViewById<EditText>(R.id.topic_comment_content_editText)
    private val commentUserImage = itemView.findViewById<ImageView>(com.interco.sotroll.R.id.topic_comment_user_image)
    private val moreIcone = itemView.findViewById<ImageView>(com.interco.sotroll.R.id.more_icone)


    override fun onBind(item: Comments, listener: TopicListener<Comments>?, index: Int) {


        if (item.user == null || item.user!!.photo == null || item.user!!.photo!!.isEmpty()) commentUserImage.setBackgroundResource(R.drawable.geek_avatar) else {
            val requestOptions = RequestOptions().apply {
                placeholder(R.drawable.geek_avatar)
                error(R.drawable.geek_avatar)
            }


            Glide.with(itemView.context)
                    .load(item.user!!.photo)
                    .apply(requestOptions)
                    .apply(RequestOptions.circleCropTransform())
                    .into(commentUserImage)
        }

        commentUserName.text = "${item.user?.nom}"

        commentContent.text = item.content

        commentContentEditText.apply {
            hint = item.content
            setHorizontallyScrolling(false)
            maxLines = 4
        }





        if (item.user?.email == currentUSer.email) {
            moreIcone.apply {
                visibility = View.VISIBLE
                setOnClickListener { showtoolsMenu(itemView.context, moreIcone, topicListener, topic, item, topicpositon) }
            }
        }
    }


    private fun showtoolsMenu(mCtx: Context, view: View, llistener: TopicListener<*>, item: Topic, commentToEdit: Comments, itemPosition: Int) {
        //creating a popup menu
        val popup = PopupMenu(mCtx, view)
        //inflating menu from xml resource
        popup.inflate(R.menu.option_menu)
        //adding click listener
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(menuitem: MenuItem): Boolean {
                when {
                    menuitem.itemId == com.interco.sotroll.R.id.spprime_item -> {
                        llistener.deleteComment(item, commentToEdit, itemPosition)
                        popup.dismiss()
                        return true
                    }
                    menuitem.itemId == com.interco.sotroll.R.id.modifier_item -> {
                        commentContentEditText.visibility = View.VISIBLE
                        commentContent.visibility = View.GONE
                        val imm = mCtx.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.showSoftInput(commentContentEditText, InputMethodManager.SHOW_IMPLICIT)
                        commentContentEditText.requestFocus()
                        commentContentEditText.setOnEditorActionListener { _, actionId, _ ->
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                commentToEdit.content = commentContentEditText.text.toString()
                                commentContentEditText.clearFocus()
                                commentContentEditText.visibility = View.GONE
                                commentContent.text = commentToEdit.content.toString()
                                commentContent.visibility = View.VISIBLE
                                KeybordUtils.hideKeyboard(mCtx as Activity)
                                llistener.editComment(item, commentToEdit, itemPosition)
                                popup.dismiss()
                                true
                            } else {
                                false
                            }
                        }

                        return true
                    }
                    menuitem.itemId == R.id.annuler_item -> {
                        popup.dismiss()
                        return true
                    }
                    else -> return false
                }


            }
        })
        //displaying the popup
        popup.show()
    }

/*
    private fun setFadeAnimation(view: View, speed: Long) {
        val anim = AlphaAnimation(0.0f, 1.0f)
        anim.duration = speed
        view.startAnimation(anim)
    }

    private fun setScaleAnimation(view: View, speed: Long) {
        val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.duration = speed
        view.startAnimation(anim)
    }

    */
}
