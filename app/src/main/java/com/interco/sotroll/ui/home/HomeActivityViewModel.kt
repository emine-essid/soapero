package com.interco.sotroll.ui.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.interco.sotroll.base.eventBus.BaseEvent
import com.interco.sotroll.base.eventBus.OfflineModeEvent
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel

open class HomeActivityViewModel(application: Application) : BaseActivityViewModel(application) {

    var hideShowFab: MutableLiveData<Boolean> = MutableLiveData()
    var hideShowOffline: MutableLiveData<Boolean> = MutableLiveData()

    init {
        hideShowFab.value = null
        hideShowOffline.value = null
    }

    override fun onBusEventReceived(event: BaseEvent) {
        super.onBusEventReceived(event)
        if (event is OfflineModeEvent) {
            if (event.isoffline) {
                hideShowOffline.postValue(true)
            } else {
                hideShowOffline.postValue(false)
            }
        } else
            if (event.nameEvent == "hideFab") {
                hideShowFab.postValue(true)
            } else {
                hideShowFab.postValue(false)

            }


    }
}
