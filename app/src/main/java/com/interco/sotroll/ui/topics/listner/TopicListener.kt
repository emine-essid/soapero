package com.interco.sotroll.ui.topics.listner

import android.view.View
import com.interco.sotroll.base.BaseRecyclerListener
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic

interface TopicListener<TYPE> : BaseRecyclerListener<TYPE> {
    fun topicLiked(item: TYPE, position: Int, view: View, isLikedOrNot: Boolean)
    fun noConnection(haveIconnection: Boolean)

    fun postComment(item: TYPE, position: Int, commentContent: String)
    fun removeEvent(item: TYPE)

    fun deleteComment(item: Topic, comment: Comments, position: Int)
    fun editComment(item: Topic, comment: Comments, position: Int)


}