package com.interco.sotroll.ui.profile.adapter

import com.interco.sotroll.base.BaseRecyclerListener

interface MesAperosListener<TYPE> : BaseRecyclerListener<TYPE> {
    fun noConnection(haveIconnection: Boolean)

    fun removeEvent(item: TYPE)


}