package com.interco.sotroll.ui.profile.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.interco.sotroll.R
import com.interco.sotroll.base.viewholder.BaseViewHolder
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.utils.ConnectivityInterceptor
import com.interco.sotroll.utils.ImageUtil
import com.ramotion.foldingcell.FoldingCell
import java.text.SimpleDateFormat


/**
 * Created by emine on 12/02/2019.
 */
open class MyAperosViewHolder(itemView: View, val currentUser: UserModel) : BaseViewHolder<Topic, MesAperosListener<Topic>>(itemView) {


    private val cell: FoldingCell = itemView.findViewById(com.interco.sotroll.R.id.folding_cell)
    @SuppressLint("SetTextI18n", "SimpleDateFormat", "CheckResult")
    override fun onBind(item: Topic, listener: MesAperosListener<Topic>?, index: Int
    ) {

        setScaleAnimation(cell, 500)
        bindFolded(item, listener)
        bindUnfolded(this, item, listener)
        cell.setOnClickListener { cell.toggle(false) }

    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    private fun bindFolded(item: Topic, listener: MesAperosListener<Topic>?) {

        val topicContentText = itemView.findViewById<TextView>(R.id.topic_content_text_profile)
        val topicContentImage = itemView.findViewById<ImageView>(R.id.topic_content_image_profile)
        val topicContentInfo = itemView.findViewById<TextView>(R.id.topic_infos_profile)
        val topicLikesImage = itemView.findViewById<ImageView>(R.id.topic_likes_image_profile)
        val topicLikesText = itemView.findViewById<TextView>(R.id.topic_likes_text_profile)
        val topicCreationDate = itemView.findViewById<TextView>(R.id.topicCreationDate_profile)
        val removeEvent = itemView.findViewById<ImageView>(com.interco.sotroll.R.id.remoce_event_profile)


        if (item.picture == null || item.picture!!.isEmpty()) {
            topicContentImage.visibility = View.GONE
        } else if (!item.picture!!.contains("no-image")) {
            Glide.with(itemView.context)
                    .load(ImageUtil.convertbase64Str(item.picture))
                    .into(topicContentImage)
        } else {
            topicContentImage.visibility = View.GONE
        }



        topicContentText.text = "${item.content}"
        val df = SimpleDateFormat("MM/dd/yyyy - HH:mm:ss")
        val dayOnly = SimpleDateFormat("MM/dd/yyyy")
        topicContentInfo.text = "${item.location}\n${df.format(item.dateRdv)}"
        topicCreationDate.text = dayOnly.format(item.createdDate)



        if (!item.nbrLikes.contains(currentUser)) {
            topicLikesImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.love_disabled))
            if (item.nbrLikes.isEmpty()) {
                topicLikesText.text = "0 personne y participe !"

            } else {

                if (item.nbrLikes.size > 1)
                    topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
                else
                    topicLikesText.text = "${item.nbrLikes.size} personne y participe !"

            }
        } else {
            if (item.nbrLikes.isEmpty()) {
                topicLikesText.text = "0 personne y participe !"

            } else {
                when {
                    item.nbrLikes.size == 1 -> topicLikesText.text = itemView.context.getString(R.string.tu_y_participe)
                    item.nbrLikes.size > 1 -> topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
                    else -> topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
                }
            }
            topicLikesImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.love_enabled))

        }


        if (item.owner!!.email == currentUser.email && ConnectivityInterceptor.isConnectedToNetwork(itemView.context)) {
            listener?.noConnection(true)
            removeEvent.visibility = View.VISIBLE
            removeEvent.setOnClickListener { listener?.removeEvent(item) }


        }
    }


    private fun setScaleAnimation(view: View, duration: Long) {
        val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.duration = duration
        anim.interpolator = LinearInterpolator()
        view.startAnimation(anim)
    }

}

@SuppressLint("CheckResult", "SimpleDateFormat", "SetTextI18n")
private fun bindUnfolded(viewHolder: MyAperosViewHolder, item: Topic, listener: MesAperosListener<Topic>?) {


    val topicUserName = viewHolder.itemView.findViewById<TextView>(R.id.topic_user_name)
    val topicUserPhoto = viewHolder.itemView.findViewById<ImageView>(R.id.topic_user_image)
    val topicContentTewt = viewHolder.itemView.findViewById<TextView>(R.id.topic_content_text)
    val topicContentImage = viewHolder.itemView.findViewById<ImageView>(R.id.topic_content_image)
    val topicContentInfos = viewHolder.itemView.findViewById<TextView>(R.id.topic_infos)
    val topicLikesImage = viewHolder.itemView.findViewById<ImageView>(R.id.topic_likes_image)
    val topicLikesText = viewHolder.itemView.findViewById<TextView>(R.id.topic_likes_text)
    val topicCreationDate = viewHolder.itemView.findViewById<TextView>(R.id.topicCreationDate)
    val removeEvent = viewHolder.itemView.findViewById<ImageView>(R.id.remoce_event)


    if (item.picture == null || item.picture!!.isEmpty()) {
        topicContentImage.visibility = View.GONE
    } else if (!item.picture!!.contains("no-image")) {
        Glide.with(viewHolder.itemView.context)
                .load(ImageUtil.convertbase64Str(item.picture))
                .apply(
                        RequestOptions()
                                .error(R.drawable.default_image_thumbnail)
                                .placeholder(R.drawable.default_image_thumbnail)
                ).into(topicContentImage)
    } else {
        topicContentImage.visibility = View.GONE
    }

    if (item.owner!!.photo == null || item.owner!!.photo!!.isEmpty()) {
        topicUserPhoto.setBackgroundResource(R.drawable.geek_avatar)
    } else {

        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.geek_avatar)
        requestOptions.error(R.drawable.geek_avatar)

        Glide.with(viewHolder.itemView.context)
                .load(item.owner!!.photo)
                .apply(requestOptions)
                .apply(RequestOptions.circleCropTransform())
                .into(topicUserPhoto)
    }

    topicUserName.text = "${item.owner?.nom}"
    topicContentTewt.text = "${item.content}"
    val df = SimpleDateFormat("MM/dd/yyyy - HH:mm:ss")
    val dayOnly = SimpleDateFormat("MM/dd/yyyy")
    topicContentInfos.text = "${item.location}\n${df.format(item.dateRdv)}"
    topicCreationDate.text = dayOnly.format(item.createdDate)



    if (!item.nbrLikes.contains(viewHolder.currentUser)) {
        topicLikesImage.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.context, R.drawable.love_disabled))
        if (item.nbrLikes.isEmpty()) {
            topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
            topicLikesText.text = viewHolder.itemView.context.getString(R.string.soit_lepremier_a_participe)
        } else {

            if (item.nbrLikes.size > 1)
                topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
            else
                topicLikesText.text = "${item.nbrLikes.size} personne y participe !"

        }
    } else {
        if (item.nbrLikes.isEmpty()) {
            topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
            topicLikesText.text = viewHolder.itemView.context.getString(R.string.soit_lepremier_a_participe)
        } else {
            if (item.nbrLikes.size == 1)
                topicLikesText.text = viewHolder.itemView.context.getString(R.string.tu_y_participe)
            else
                if (item.nbrLikes.size > 1)
                    topicLikesText.text = "${item.nbrLikes.size} personnes y participent !"
                else
                    topicLikesText.text = "${item.nbrLikes.size} personne y participe !"
        }
        topicLikesImage.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.context, R.drawable.love_enabled))

    }


    if (item.owner!!.email == viewHolder.currentUser.email && ConnectivityInterceptor.isConnectedToNetwork(viewHolder.itemView.context)) {
        listener?.noConnection(true)
        removeEvent.visibility = View.VISIBLE
        removeEvent.setOnClickListener { listener?.removeEvent(item) }


    }

}
