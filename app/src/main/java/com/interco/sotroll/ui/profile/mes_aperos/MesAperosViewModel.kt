package com.interco.sotroll.ui.profile.mes_aperos

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.interco.sotroll.R
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import io.reactivex.functions.Consumer
import timber.log.Timber
import java.util.*

class MesAperosViewModel(application: Application) : BaseActivityViewModel(application) {

    var topiclist: MutableLiveData<ArrayList<Topic>> = MutableLiveData()
    var topiclistError: MutableLiveData<String>? = MutableLiveData()
    var isOnline: MutableLiveData<Boolean> = MutableLiveData()
    var currentUser: UserModel = Gson().fromJson(preferencesHelper.getString("currentUser"), UserModel::class.java)

    init {

        isOnline.value = null
        getAllTopics()
    }


    private fun getAllTopics() {
        subscribeMainThred(networkApiService.getAllTopics(), Consumer { result: List<Topic> ->
            toggleLoading.postValue(true)
            topiclist.postValue(result as ArrayList<Topic>)
            toggleLoading.postValue(false)

            isOnline.postValue(true)

        }, Consumer { it ->
            toggleLoading.postValue(false)
            if (it.message == getString(R.string.error_no_network) || it.message!!.contains("failed")) {
                isOnline.postValue(false)
            }

            topiclistError!!.postValue("ERROR  FINDING TOPICS IN COULD  " + it.message)

        })
    }

    fun removeApero( id : Long) {
        toggleLoading.postValue(true)

        subscribeMainThred(networkApiService.removeTopic(id), Consumer {
            toggleLoading.postValue(false)
            Timber.e("deleted !")
        }, Consumer { it ->
            getAllTopics()
            if (it.message == getString(R.string.error_no_network)) {
                isOnline.postValue(false)
            } else {
                Timber.e("ERROR Removing TOPIC {${it.message}}")
            }
        })
    }






}
