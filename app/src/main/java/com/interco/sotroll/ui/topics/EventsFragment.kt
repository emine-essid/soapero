package com.interco.sotroll.ui.topics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.interco.sotroll.R
import com.interco.sotroll.base.eventBus.BaseEvent
import com.interco.sotroll.base.eventBus.OfflineModeEvent
import com.interco.sotroll.base.eventBus.RxBus
import com.interco.sotroll.base.fragment.BaseFragment
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.ui.topics.adapter.topics.NewTopicAdapter
import com.interco.sotroll.ui.topics.listner.TopicListener
import com.interco.sotroll.utils.DialogUtils
import com.interco.sotroll.utils.KeybordUtils
import kotlinx.android.synthetic.main.fragment_new.*


class EventsFragment : BaseFragment<EventFragmentViewModel>() {

    private var myOldList: MutableList<Topic> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val myRecycler = view.findViewById<RecyclerView>(R.id.new_topics)
        val manager = LinearLayoutManager(getActivity())
        myRecycler.layoutManager = manager
        myRecycler.isNestedScrollingEnabled = true
        val adapter = NewTopicAdapter(getActivity()!!.baseContext, viewModel!!.currentUser)
        adapter.setListener(object : TopicListener<Topic> {

            override fun noConnection(haveIconnection: Boolean) {
            }

            override fun removeEvent(item: Topic) {
                DialogUtils.showActionDialog(getActivity()!!, " Voulez-vous vraiment supprimer cet éventement?", "Yes", "No", Runnable {
                    viewModel?.removeElement(item.id!!)

                })
            }

            override fun topicLiked(item: Topic, position: Int, view: View, isLikedOrNot: Boolean) {

                if (!isLikedOrNot) {
                    item.nbrLikes.add(viewModel!!.currentUser)
                    (view as ImageView).setImageDrawable(ContextCompat.getDrawable(getActivity()!!.baseContext, R.drawable.love_enabled))
                    viewModel?.likeTopic(item.id!!)
                    adapter.notifyItemChanged(position)

                } else {
                    item.nbrLikes.remove(viewModel!!.currentUser)
                    (view as ImageView).setImageDrawable(ContextCompat.getDrawable(getActivity()!!.baseContext, R.drawable.love_disabled))
                    viewModel?.unLikeTopic(item.id!!)
                    adapter.notifyItemChanged(position)

                }
            }

            override fun postComment(item: Topic, position: Int, commentContent: String) {
                val commentList: MutableList<Comments> = ArrayList()
                commentList.addAll(item.commentList)
                commentList.add(0, Comments(0, viewModel!!.currentUser, commentContent))
                item.commentList.clear()
                item.commentList.addAll(commentList)
                adapter.notifyItemChanged(position)

                viewModel?.postComment(item.id!!, commentContent)
                KeybordUtils.hideKeyboard(getActivity())

            }

            override fun deleteComment(item: Topic, comment: Comments, position: Int) {
                item.commentList.remove(comment)
                adapter.notifyItemChanged(position)
                viewModel!!.deleteComment(comment)
            }

            override fun editComment(item: Topic, comment: Comments, position: Int) {
                viewModel!!.updateComment(comment.id!!, comment.content!!)
            }
        })

        viewModel!!.topiclist.observe(this, Observer {
            if (it != null && !it.isEmpty()) {
                adapter.clear()
                myOldList.clear()
                myOldList.addAll(it)
                adapter.setData(it)
                myRecycler.adapter = adapter
            }
        })

        viewModel!!.isOnline.observe(this, Observer { isOnline ->
            if (isOnline != null && isOnline)
                RxBus.getInstance().send(OfflineModeEvent("Event_is_online", true)) else RxBus.getInstance().send(OfflineModeEvent("Event_is_online", false))

        })

        myRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                // We have reached the end of the recycler view.
                if (!adapter.getItems().isNullOrEmpty()
                        && adapter.getItems()!!.size > 2
                        && manager.findLastVisibleItemPosition() == Math.abs(manager.itemCount - 1))

                    RxBus.getInstance().send(BaseEvent("hideFab"))
                else
                    RxBus.getInstance().send(BaseEvent("showFab"))

                super.onScrolled(recyclerView, dx, dy)
            }
        })

        new_serch_bar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                adapter.filter.filter(query)
                return false
            }

        })
        new_serch_bar.setOnCloseListener {
            with(adapter) {
                setData(myOldList)
                notifyDataSetChanged()
            }

            return@setOnCloseListener false
        }
    }


}

