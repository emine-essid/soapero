package com.interco.sotroll.ui.login

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.gson.Gson
import com.interco.sotroll.BuildConfig
import com.interco.sotroll.R
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.database.entity.CommentsData
import com.interco.sotroll.database.entity.TopicsData
import com.interco.sotroll.models.UserModel
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*

/**
 * Created by emine on 07/02/2019.
 */
internal class SplashViewModel(application: Application) : BaseActivityViewModel(application) {
    var currentUser: MutableLiveData<UserModel> = MutableLiveData()

    var canPass: MutableLiveData<String> = MutableLiveData()
    var showError: MutableLiveData<String> = MutableLiveData()

    init {
        canPass.value = ""
        showError.value = ""

        currentUser.value = Gson().fromJson(preferencesHelper.getString("currentUser"), UserModel::class.java)

    }


    fun shouldIDPass(account: GoogleSignInAccount?) {

        if (account?.email != null && account.email!!.contains("@soat.fr")) {
            val currentUser = UserModel(null, account.displayName, account.email
                    ?: "", account.photoUrl.toString(), false)

            subscribeMainThred(networkApiService.addUSer(currentUser), Consumer {
                preferencesHelper.putString("currentUser", Gson().toJson(it))

                canPass.postValue(account.email)
            }, Consumer {

                if (BuildConfig.DEBUG && it.message!!.contains("failed")) {
                    preferencesHelper.putString("currentUser", Gson().toJson(currentUser))

                    canPass.postValue(account.email)
                } else {
                    showError.postValue(it.message)
                }
            })

        } else {
            showError.postValue(getString(R.string.error_acces_denied))

        }


    }

    fun postuser(user: UserModel) {
        subscribeMainThred(networkApiService.addUSer(user), Consumer {
        }, Consumer {
            showError.postValue(it.message)
        })

    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun populateMe(me: UserModel) {
        val commentor = UserModel(0, "Super User ", "https://phoxis.files.wordpress.com/2009/09/ff_logo.png", "emine.essid@soat.fr")

        val comment1 = CommentsData(0, commentor, " mmwé bof ! \uD83E\uDD65")
        val comment2 = CommentsData(0, commentor, " cool ! \uD83C\uDF4C \uD83C\uDF4C \uD83C\uDF4C")
        val comment3 = CommentsData(0, commentor, " \uD83C\uDF46 ")

        val listcomm: MutableList<CommentsData> = ArrayList()
        listcomm.add(comment2)
        listcomm.add(comment1)
        listcomm.add(comment3)

        val likesList: MutableList<UserModel> = ArrayList()
        likesList.add(UserModel(0, "SOAT APERO", "https://corfigas.com/wp-content/uploads/2018/10/1481722165_82979_22130_7152.png", "soapero@soat.fe"))


        val k = TopicsData(0,
                me, null,
                "An apéritif is an alcoholic beverage usually served before a meal to stimulate the appetite, and is therefore usually dry rather than sweet. Common choices for an apéritif are vermouth; champagne; pastis; gin; rakı; fino, amontillado or other styles of dry sherry (but not usually cream or oloroso sherry, which is very sweet and rich); and any still, dry, light white wine ",
                "Paris 75013 - SOAT", Date(), Date(Date().getTime() + (1000 * 60 * 60 * 24)), ArrayList(), listcomm)


        Completable.fromAction {
            dataBaseHelper.TopicsDataBase().insert(k)
        }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {
                        Timber.e(" Disposable !%s", d.isDisposed)
                    }

                    override fun onComplete() {
                        Timber.e(" YESSSSSS !")
                    }

                    override fun onError(e: Throwable) {
                        Timber.e(" oh God why !%s", e)
                    }
                })

    }

}
