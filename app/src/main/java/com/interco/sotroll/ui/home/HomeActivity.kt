package com.interco.sotroll.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.transition.Explode
import android.transition.Fade
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import com.iammert.library.readablebottombar.ReadableBottomBar
import com.interco.sotroll.R
import com.interco.sotroll.base.activity.BaseActivity
import com.interco.sotroll.ui.create_topic.CreateTopicActivity
import com.interco.sotroll.ui.profile.ProfileFragment
import com.interco.sotroll.ui.topics.EventsFragment
import com.interco.sotroll.utils.MyBounceInterpolator
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity<HomeActivityViewModel>() {


    private var canIDoSomething: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.apply {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
// set an enter transition
            enterTransition = Explode()
// set an exit transition
            exitTransition = Fade()
            enterTransition.excludeTarget(bottom_navigation, true)

            canIDoSomething = false

        }


        setContentView(R.layout.activity_home)


        addFragment(provideFragmentManager(), EventsFragment(), R.id.fragment_container, true)


        bottom_navigation.setOnItemSelectListener(object : ReadableBottomBar.ItemSelectListener {
            override fun onItemSelected(index: Int) {
                if (index == 0)
                    navigateToSelectedFragment(1)
                else
                    navigateToSelectedFragment(2)

            }
        })



        viewModel!!.hideShowFab.observe(this, Observer
        {
            if (it != null) {
                if (it) {
                    floatingActionButton.animate().translationX(150F).setInterpolator(LinearInterpolator()).start()
                } else {
                    floatingActionButton.animate().translationX(-50F).setInterpolator(LinearInterpolator()).start()
                }
            }
        })

        viewModel!!.hideShowOffline.observe(this, Observer
        {
            if (it != null) {
                if (!it) {
                    canIDoSomething = false
                    no_connection_text.visibility = View.VISIBLE
                } else {
                    canIDoSomething = true
                    no_connection_text.visibility = View.GONE
                }
            }
        })


        floatingActionButton.setOnClickListener {
            if (canIDoSomething) {
                val intent = Intent(this, CreateTopicActivity::class.java)


                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                        androidx.core.util.Pair<View, String>(bottom_navigation, "addApero_btn_purple")
                        // androidx.core.util.Pair<View, String>(floatingActionButton, "image_user")
                )
                startActivityForResult(intent, 200, options.toBundle())
            } else {
                val myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce)
                // Use bounce interpolator with amplitude 0.1 and frequency 15
                val interpolator = MyBounceInterpolator(0.1, 15.0)
                myAnim.interpolator = interpolator
                no_connection_text.startAnimation(myAnim)
            }
        }
    }

    fun navigateToSelectedFragment(fragmentNumber: Int) {

        when (fragmentNumber) {
            1 -> {
                replaceFragmentNoAddToStack(provideFragmentManager(), EventsFragment(), R.id.fragment_container, true)
            }
            2 -> {
                replaceFragmentNoAddToStack(provideFragmentManager(), ProfileFragment(), R.id.fragment_container, true)
            }
            else -> {
                throw NullPointerException("WTF IS HAPPNING !")
            }

        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            replaceFragmentNoAddToStack(provideFragmentManager(), EventsFragment(), R.id.fragment_container, true)
            Toast.makeText(this, "Topic Created", Toast.LENGTH_SHORT).show()
        }
    }


}
