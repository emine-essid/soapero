package com.interco.sotroll.NETWORK

import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by emine on 14/12/2018.
 */
interface NetworkApi {

    @Headers("Content-Type: application/json")
    @GET(BaseRoutes.GET_ALL_TOPICS)
    fun getAllTopics(): Observable<java.util.ArrayList<Topic>>

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.ADD_USER)
    fun addUser(@Query("nom") nom: String,
                @Query("email") email: String,
                @Query("photo") photo: String,
                @Query("role") role: Boolean = false

    ): Observable<UserModel>

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.ADD_TOPIC)
    fun addTopic(@Query("userId") userId: Long,
                 @Body topic: Topic): Observable<Topic>

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.POST_COMMENT)
    fun addComment(@Query("userId") userId: Long,
                   @Query("topicId") topicId: Long,
                   @Query("content") content: String

    ): Observable<Comments>


    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.EDIT_COMMENT)
    fun updateComment(
            @Query("idComment") idComment: Long,
            @Query("content") content: String

    ): Observable<Comments>

    @Headers("Content-Type: application/json")
    @DELETE(BaseRoutes.DEL_COMMENT)
    fun deleteComment(
            @Query("idComment") idComment: Long

    ): Completable

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.LIKE_TOPIC)
    fun likeTopic(
            @Query("idTopic") idTopic: Long,
            @Query("idUser") idUser: Long
    ): Observable<Topic>

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.UNLIKE_TOPIC)
    fun unlikeTopic(
            @Query("idTopic") idTopic: Long,
            @Query("idUser") idUser: Long
    ): Observable<Topic>

    @Headers("Content-Type: application/json")
    @DELETE(BaseRoutes.DEL_TOPIC)
    fun removeTopic(
            @Query("id") id: Long
    ): Observable<Void>


}
