package com.interco.sotroll.NETWORK

import com.google.gson.Gson
import com.interco.sotroll.models.Comments
import com.interco.sotroll.models.Topic
import com.interco.sotroll.models.UserModel
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by emine on 14/12/2018.
 */
@Singleton
open class NetworkApiService @Inject
constructor(internal var gson: Gson, retrofit: Retrofit) {
    private val networkApi: NetworkApi = retrofit.create(NetworkApi::class.java)

    fun getAllTopics(): Observable<java.util.ArrayList<Topic>> {
        return networkApi.getAllTopics()
    }

    fun addUSer(userModel: UserModel): Observable<UserModel> {
        return networkApi.addUser(userModel.nom!!, userModel.email!!, userModel.photo!!, false)
    }

    fun addTopic(topic: Topic, userid: Long): Observable<Topic> {
        return networkApi.addTopic(userid, topic)
    }

    fun addComment(commentID: Long, commentContent: String, userid: Long): Observable<Comments> {
        return networkApi.addComment(userid, commentID, commentContent)
    }

    fun updateComment(commentID: Long, commentContent: String): Observable<Comments> {
        return networkApi.updateComment(commentID, commentContent)
    }

    fun deleteComment(commentID: Long): Completable {
        return networkApi.deleteComment(commentID)
    }

    fun likeTopic(id: Long , userId:Long): Observable<Topic> {
        return networkApi.likeTopic(id,userId)
    }

    fun removeTopic(id: Long): Observable<Void> {
        return networkApi.removeTopic(id)
    }

    fun unlikeTopic(id: Long,userId:Long): Observable<Topic> {
        return networkApi.unlikeTopic(id,userId)
    }


}
