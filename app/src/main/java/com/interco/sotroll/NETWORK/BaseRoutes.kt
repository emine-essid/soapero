package com.interco.sotroll.NETWORK

/**
 * Created by emine on 04/02/2019.
 */
open class BaseRoutes {
    companion object {
        const val GET_ALL_TOPICS = "getAllTopics"
        const val ADD_USER = "addUser"
        const val ADD_TOPIC = "addTopic?"
        const val POST_COMMENT = "addComment"
        const val EDIT_COMMENT = "editComment"
        const val DEL_COMMENT = "deleteComment"
        const val LIKE_TOPIC = "likeTopic"
        const val UNLIKE_TOPIC = "unlikeTopic"
        const val DEL_TOPIC = "deleteTopic"
    }
}