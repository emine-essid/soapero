package com.interco.sotroll.base.eventBus

import androidx.annotation.Keep

@Keep
open class BaseEvent(nameEvent: String) {
    var nameEvent: String
        protected set

    init {
        this.nameEvent = nameEvent
    }
}