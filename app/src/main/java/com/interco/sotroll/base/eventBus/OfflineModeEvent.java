package com.interco.sotroll.base.eventBus;

import org.jetbrains.annotations.NotNull;

/**
 * Created by emine on 21/02/2019.
 */
public class OfflineModeEvent extends BaseEvent {
    private Boolean isoffline = false;

    public OfflineModeEvent(@NotNull String nameEvent, Boolean isoffline) {
        super(nameEvent);
        this.isoffline = isoffline;
    }

    public Boolean getIsoffline() {
        return isoffline;
    }

    public void setIsoffline(Boolean isoffline) {
        this.isoffline = isoffline;
    }
}
