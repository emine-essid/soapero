package com.interco.sotroll.base.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import java.lang.reflect.ParameterizedType


abstract class BaseActivity<T : BaseActivityViewModel> : BaseBaseActivity() {

    protected var viewModel: T? = null
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        initObservers()
    }

    private fun initViewModel() {
        val viewModelClass = getTypeParameterClass<T>(javaClass, 0)
        viewModel = ViewModelProviders.of(this).get(viewModelClass)
        viewModel!!.onCreated()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (viewModel != null)
                viewModel?.onActivityResult(requestCode, resultCode, data!!)
        } catch (e: Exception) {

        }

    }

    override fun onStart() {
        super.onStart()
        if (viewModel != null)
            viewModel!!.onStart()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel != null)
            viewModel!!.onResume()
    }

    override fun onPause() {
        if (viewModel != null)
            viewModel!!.onPause()
        super.onPause()
    }

    override fun onStop() {
        if (viewModel != null)
            viewModel!!.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        if (viewModel != null)
            viewModel!!.onDestroy()
        super.onDestroy()
    }

    private fun initObservers() {
        viewModel!!.toggleLoading.observe(this, Observer {
            this.toggleLoading(it) })

        viewModel!!.recreate.observe(this, Observer {
            this@BaseActivity.startActivity(this@BaseActivity.intent)
            this@BaseActivity.finish()
            this@BaseActivity.overridePendingTransition(0, 0)
        })

    }

    companion object {

        fun <T> getTypeParameterClass(clz: Class<*>, argsIndex: Int): Class<T> {
            val type = clz.genericSuperclass
            val paramType = type as ParameterizedType
            return paramType.actualTypeArguments[argsIndex] as Class<T>
        }
    }



}
