package com.interco.sotroll.base.activity


import android.app.ProgressDialog
import android.app.ProgressDialog.show
import android.os.Bundle
import android.view.KeyEvent
import androidx.annotation.AnimRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.interco.sotroll.base.fragment.BaseFragment
import com.interco.sotroll.utils.KeybordUtils
import timber.log.Timber

/**
 * Created by Hatem Toumi on 10/22/18.
 */
open abstract class BaseBaseActivity : AppCompatActivity() {

    private lateinit var dialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dialog = show(this, "",
                "Loading. Please wait...", true)


        toggleLoading(false)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && manageBackClick()) {
            true
        } else super.onKeyDown(keyCode, event)
    }

    protected fun manageBackClick(): Boolean {
        var backPressConsumed = false

        if (dialog.isShowing) {
            toggleLoading(false)
            backPressConsumed = true
        }
        return backPressConsumed
    }


    fun provideFragmentManager(): FragmentManager = supportFragmentManager


    fun toggleLoading(show: Boolean) {
            if (show) {
                dialog.show()
            } else {
                dialog.dismiss()
            }

    }


    //    ********************** fragments *****************

    open fun getFragment(fragmentManager: FragmentManager, Object: Class<*>): BaseFragment<*> {
        return (fragmentManager.findFragmentByTag(Object.name) as BaseFragment<*>?)!!
    }

    fun getBackStackEntryCount(fragmentManager: FragmentManager?): Int {
        return fragmentManager?.backStackEntryCount ?: 0
    }


    fun replaceFragment(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            val className = fragment.javaClass.name
            val ft = fragmentManager.beginTransaction()
            try {
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded) {
                    ft.replace(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                }
            } catch (e: Exception) {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
            }


        }
    }

    fun replaceFragmentNoAddToStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            val ft = fragmentManager.beginTransaction()
            try {
                val className = fragment.javaClass.name
                if (!fragment.isAdded()) {
                    ft.replace(frameId, fragment, className)
                } else {
                    ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                }
            } catch (e: Exception) {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                Timber.e("replaceFragmentNoAddToStack  : " + e)
            }

            ft.commit()
        }
    }

    fun replaceFragmentNoAddToStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            val ft = fragmentManager.beginTransaction()
            ft.setCustomAnimations(enter, exit, popEnter, popExit)
            try {
                val className = fragment.javaClass.name
                if (!fragment.isAdded()) {
                    ft.replace(frameId, fragment, className)
                } else {
                    ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                }
            } catch (e: Exception) {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                Timber.e("replaceFragmentNoAddToStack  : " + e)
            }

            ft.commit()
        }
    }

    fun addFragment(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            try {


                val className = fragment.javaClass.name
                val ft = fragmentManager.beginTransaction()
                if (!fragment.isAdded()) {
                    ft.add(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                } else {
                    ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                }
            } catch (e: Exception) {
                Timber.e("addFragment  : " + e)
            }

        }
    }

    fun addFragment(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            val ft = fragmentManager.beginTransaction()
            try {
                val className = fragment.javaClass.name
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded()) {
                    ft.add(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                }
            } catch (e: Exception) {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
            }
        }

    }


    fun addFragmentWithoutAddToStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            try {
                val className = fragment.javaClass.name
                val ft = fragmentManager.beginTransaction()
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded()) {
                    ft.add(frameId, fragment, className)
                    ft.commit()
                } else {
                    ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                }
            } catch (e: Exception) {
                Timber.e("addFragmentWithoutAddToStack  : " + e)
            }

        }
    }

    fun addFragmentWithoutAddToStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            try {
                val className = fragment.javaClass.name
                val ft = fragmentManager.beginTransaction()
                if (!fragment.isAdded()) {
                    ft.add(frameId, fragment, className)
                    ft.commit()
                } else {
                    ft.show(this.getFragment(fragmentManager, fragment.javaClass))
                }
            } catch (e: Exception) {
                Timber.e("addFragmentWithoutAddToStack  : " + e)
            }

        }
    }

    fun removeFragment(fragmentManager: FragmentManager, fragment: BaseFragment<*>, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            val ft = fragmentManager.beginTransaction()
            ft.remove(fragment)
            ft.commit()
        }
    }

    fun addFragmentToClearStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            this.clearFullStack(fragmentManager, isActivityRunning)
            val className = fragment.javaClass.name
            val ft = fragmentManager.beginTransaction()
            if (!fragment.isAdded()) {
                ft.replace(frameId, fragment, className)
                ft.addToBackStack(className)
                ft.commit()
            } else {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
            }

        }
    }

    fun addFragmentToClearStack(fragmentManager: FragmentManager, fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(this)
            this.clearFullStack(fragmentManager, isActivityRunning)
            val className = fragment.javaClass.name
            val ft = fragmentManager.beginTransaction()
            ft.setCustomAnimations(enter, exit, popEnter, popExit)
            if (!fragment.isAdded()) {
                ft.replace(frameId, fragment, className)
                ft.addToBackStack(className)
                ft.commit()
            } else {
                ft.show(this.getFragment(fragmentManager, fragment.javaClass))
            }

        }
    }

    fun clearFullStack(fragmentManager: FragmentManager, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            fragmentManager.popBackStack(null, 1)
        }
    }


}
