package com.interco.sotroll.base.fragment


import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.AnimRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.interco.sotroll.base.activity.BaseActivity
import com.interco.sotroll.base.viewmodel.BaseActivityViewModel
import com.interco.sotroll.utils.KeybordUtils
import timber.log.Timber

abstract class BaseFragment<T : BaseActivityViewModel> : Fragment() {

    protected lateinit var activity: BaseActivity<*>
    protected var viewModel: T? = null
        private set

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelProviders.of(this).get(BaseActivity.getTypeParameterClass(javaClass, 0))
        activity = context as BaseActivity<*>
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        if (viewModel != null)
            viewModel!!.onCreated()
    }

    override fun onStart() {
        if (viewModel != null)
            viewModel!!.onStart()
        super.onStart()
    }

    override fun onResume() {
        if (viewModel != null)
            viewModel!!.onResume()
        super.onResume()
    }

    override fun onPause() {
        if (viewModel != null)
            viewModel!!.onPause()
        super.onPause()
    }

    override fun onStop() {
        if (viewModel != null)
            viewModel!!.onStop()
        super.onStop()
    }

    override fun onDestroyView() {
        if (viewModel != null)
            viewModel!!.onDestroy()
        super.onDestroyView()
    }

    protected fun toggleLoading(show: Boolean) {
        if (isAdded) {
            activity.toggleLoading(show)
        }
    }

    private fun initObservers() {
        viewModel!!.toggleLoading.observe(this, Observer<Boolean> { this.toggleLoading(it) })
    }

//    fragments


    open fun getFragment(Object: Class<*>): BaseFragment<*> {
        return (childFragmentManager.findFragmentByTag(Object.name) as BaseFragment<*>?)!!
    }

    fun getBackStackEntryCount(): Int {
        return fragmentManager?.backStackEntryCount ?: 0
    }


    fun replaceFragment(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            val className = fragment.javaClass.name
            val ft = childFragmentManager.beginTransaction()
            try {
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded) {
                    ft.replace(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                }
            } catch (e: Exception) {
                Timber.e(e.message)
            }


        }
    }

    fun replaceFragmentNoAddToStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            val ft = childFragmentManager.beginTransaction()
            try {
                val className = fragment.javaClass.name
                if (!fragment.isAdded) {
                    ft.replace(frameId, fragment, className)
                } else {
                    ft.show(fragment)

                }
            } catch (e: Exception) {
                Timber.e("replaceFragmentNoAddToStack  : " + e)
            }

            ft.commit()
        }
    }

    fun replaceFragmentNoAddToStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            val ft = childFragmentManager.beginTransaction()
            ft.setCustomAnimations(enter, exit, popEnter, popExit)
            try {
                val className = fragment.javaClass.name
                if (!fragment.isAdded) {
                    ft.replace(frameId, fragment, className)
                } else {
                    ft.show(fragment)

                }
            } catch (e: Exception) {
                ft.show(fragment)

                Timber.e("replaceFragmentNoAddToStack  : " + e)
            }

            ft.commit()
        }
    }

    fun addFragment(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            try {


                val className = fragment.javaClass.name
                val ft = childFragmentManager.beginTransaction()
                if (!fragment.isAdded) {
                    ft.add(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                } else {
                    ft.show(fragment)
                }
            } catch (e: Exception) {
                Timber.e("addFragment  : " + e)
            }

        }
    }

    fun addFragment(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            val ft = childFragmentManager.beginTransaction()
            try {
                val className = fragment.javaClass.name
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded) {
                    ft.add(frameId, fragment, className)
                    ft.addToBackStack(className)
                    ft.commit()
                }
            } catch (e: Exception) {
                ft.show(fragment)
            }
        }

    }


    fun addFragmentWithoutAddToStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            try {
                val className = fragment.javaClass.name
                val ft = childFragmentManager.beginTransaction()
                ft.setCustomAnimations(enter, exit, popEnter, popExit)
                if (!fragment.isAdded) {
                    ft.add(frameId, fragment, className)
                    ft.commit()
                } else {
                    ft.show(fragment)
                }
            } catch (e: Exception) {
                Timber.e("addFragmentWithoutAddToStack  : " + e)
            }

        }
    }

    fun addFragmentWithoutAddToStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            try {
                val className = fragment.javaClass.name
                val ft = childFragmentManager.beginTransaction()
                if (!fragment.isAdded) {
                    ft.add(frameId, fragment, className)
                    ft.commit()
                } else {
                    ft.show(fragment)
                }
            } catch (e: Exception) {
                Timber.e("addFragmentWithoutAddToStack  : " + e)
            }

        }
    }

    fun removeFragment(fragment: BaseFragment<*>, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            val ft = childFragmentManager.beginTransaction()
            ft.remove(fragment)
            ft.commit()
        }
    }

    fun addFragmentToClearStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            this.clearFullStack(isActivityRunning)
            val className = fragment.javaClass.name
            val ft = childFragmentManager.beginTransaction()
            if (!fragment.isAdded) {
                ft.replace(frameId, fragment, className)
                ft.addToBackStack(className)
                ft.commit()
            } else {
                ft.show(fragment)
            }

        }
    }

    fun addFragmentToClearStack(fragment: BaseFragment<*>, frameId: Int, isActivityRunning: Boolean, @AnimRes enter: Int, @AnimRes exit: Int, @AnimRes popEnter: Int, @AnimRes popExit: Int) {
        if (isActivityRunning) {
            KeybordUtils.hideKeyboard(activity)
            this.clearFullStack(isActivityRunning)
            val className = fragment.javaClass.name
            val ft = childFragmentManager.beginTransaction()
            ft.setCustomAnimations(enter, exit, popEnter, popExit)
            if (!fragment.isAdded) {
                ft.replace(frameId, fragment, className)
                ft.addToBackStack(className)
                ft.commit()
            } else {
                ft.show(fragment)
            }

        }
    }

    fun clearFullStack(isActivityRunning: Boolean) {
        if (isActivityRunning) {
            childFragmentManager.popBackStack(null, 1)
        }
    }


}
