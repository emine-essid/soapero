package com.interco.sotroll.base.viewmodel


import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.interco.sotroll.NETWORK.NetworkApiService
import com.interco.sotroll.base.eventBus.BaseEvent
import com.interco.sotroll.base.eventBus.RxBus
import com.interco.sotroll.database.TopicsDataBase
import com.interco.sotroll.root.App
import com.interco.sotroll.root.App.Companion.appComponant
import com.interco.sotroll.utils.PreferencesHelper
import com.interco.sotroll.utils.SingleLiveEvent
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by emine on 14/12/2018.
 */
open class BaseActivityViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {


    var networkApiService: NetworkApiService

    var preferencesHelper: PreferencesHelper

    var dataBaseHelper: TopicsDataBase


    var toggleLoading: MutableLiveData<Boolean> = MutableLiveData()
    var recreate: SingleLiveEvent<Boolean> = SingleLiveEvent()


    //RX
    private val subscriptions = CompositeDisposable()
    //RX BUS
    private val busEventSubscriptions = CompositeDisposable()

    init {
        App.appComponant!!.inject(this)
        networkApiService = App.appComponant!!.provideApiService()
        preferencesHelper = PreferencesHelper(App.appComponant!!.sharedPrefsManager())
        dataBaseHelper = App.appComponant!!.provideDatabase()
    }


    //    -------------

    private fun sub(s: Disposable) {
        this.subscriptions.add(s)
    }

    protected fun clearSubs() {
        this.subscriptions.clear()
    }

    protected fun <R> subscribe(observable: Observable<R>, onNext: Consumer<in R>, onError: Consumer<Throwable>, onCompleted: Action) {
        this.sub(observable.subscribe(onNext, onError, onCompleted))
    }

    protected fun <R> subscribeMainThred(observable: Observable<R>, onNext: Consumer<in R>, onError: Consumer<Throwable>?) {
        subscriptions.add(
                observable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(onNext, onError!!))
    }


    protected fun <R> subscribe(observable: Observable<R>, onNext: Consumer<in R>) {
        this.subscribeMainThred(observable, onNext, null)
    }

    protected fun subscribe(completable: Completable, onComplete: Action, onError: Consumer<in Throwable>?) {
        subscriptions.add(completable.doOnSubscribe { this.sub(it) }.subscribe(onComplete, onError!!))
    }

    protected fun subscribeCompletable(completable: Completable, onComplete: Action, onError: Consumer<in Throwable>?) {
        subscriptions.add(completable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { this.sub(it) }
                .subscribe(onComplete, onError!!))
    }

    protected fun subscribe(completable: Completable, onComplete: Action) {
        this.subscribe(completable, onComplete, null)
    }

    private fun subscribeBusEvents() {
        val observable = RxBus.getInstance().toObservable().doOnNext { this.onBusEventReceived(it) }
        this.busEventSubscriptions.add(observable.subscribeOn(AndroidSchedulers.mainThread()).subscribe())
    }

    private fun unsubscribeBusEvents() {
        this.busEventSubscriptions.clear()
    }

    fun getString(int: Int): String {
        return appComponant!!.provideConext().getString(int)
    }

    protected open fun onBusEventReceived(event: BaseEvent) {}

    //--- activity life cycle


    fun onCreated() {
        this.subscribeBusEvents()
    }

    fun onStart() {}

    fun onResume() {}

    fun onPause() {
        toggleLoading.value = false
        this.clearSubs()
    }

    fun onStop() {}

    fun onDestroy() {
        this.unsubscribeBusEvents()

    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {}


}
