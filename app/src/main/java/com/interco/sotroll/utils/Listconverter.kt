package com.interco.sotroll.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.interco.sotroll.database.entity.CommentsData
import com.interco.sotroll.models.UserModel
import java.util.*

/**
 * Created by emine on 31/01/2019.
 */
open class Listconverter {


    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }


    @TypeConverter
    fun stringToUserModez(json: String): List<UserModel>? {
        val gson = Gson()
        val type = object : TypeToken<List<UserModel>>() {

        }.type
        return gson.fromJson<List<UserModel>>(json, type)
    }

    @TypeConverter
    fun userModelToString(list: List<UserModel>): String {
        val gson = Gson()
        val type = object : TypeToken<List<UserModel>>() {

        }.type
        return gson.toJson(list, type)
    }


    @TypeConverter
    fun stringToComments(json: String): List<CommentsData>? {
        val gson = Gson()
        val type = object : TypeToken<List<CommentsData>>() {

        }.type
        return gson.fromJson<List<CommentsData>>(json, type)
    }

    @TypeConverter
    fun commentsToString(list: List<CommentsData>): String {
        val gson = Gson()
        val type = object : TypeToken<List<CommentsData>>() {

        }.type
        return gson.toJson(list, type)
    }


    @TypeConverter
    fun stringToUserModel(json: String): UserModel? {
        val gson = Gson()
        val type = object : TypeToken<UserModel>() {

        }.type
        return gson.fromJson<UserModel>(json, type)
    }

    @TypeConverter
    fun userModelToString(userModel: UserModel): String {
        val gson = Gson()
        val type = object : TypeToken<UserModel>() {

        }.type
        return gson.toJson(userModel, type)
    }


}
