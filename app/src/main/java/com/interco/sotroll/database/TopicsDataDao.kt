package com.interco.sotroll.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.interco.sotroll.database.entity.TopicsData
import io.reactivex.Observable


/**
 * Created by emine on 31/01/2019.
 */
@Dao
interface TopicsDataDao {
    @Query("SELECT * from topicsData ORDER BY createdDate DESC")
    fun getTopics(): Observable<List<TopicsData>>

    @Insert(onConflict = REPLACE)
    fun insert(weatherData: TopicsData)

    @Insert(onConflict = REPLACE)
    fun insertMultipleTopics(topicList: List<TopicsData>)

    @Query("DELETE from topicsData")
    fun deleteAll()
}