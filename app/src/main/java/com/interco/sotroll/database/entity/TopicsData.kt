package com.interco.sotroll.database.entity

import androidx.annotation.NonNull
import androidx.room.*
import com.interco.sotroll.models.UserModel
import com.interco.sotroll.utils.Listconverter
import java.util.*

/**
 * Created by emine on 31/01/2019.
 */
@Entity(tableName = "topicsData")
@TypeConverters(Listconverter::class)
data class TopicsData(

        @PrimaryKey(autoGenerate = true) var topicsId: Long,
        @Embedded(prefix = "topic_") var creator: UserModel = UserModel(null, "", "", ""),
        @ColumnInfo(name = "picture") var imageUrl: String?,
        @ColumnInfo(name = "content") var content: String,
        @ColumnInfo(name = "location") var location: String,
        @NonNull
        @ColumnInfo(name = "createdDate") var date_creation: Date,
        @ColumnInfo(name = "dateRdv") var date_RDV: Date?,
        @ColumnInfo(name = "likes") var likes: MutableList<UserModel> = ArrayList(),
        //@Relation(parentColumn = "topicsId", entityColumn = "id", entity = CommentsData::class)
        @ColumnInfo(name = "comments_")
        var comments: MutableList<CommentsData> = ArrayList()


) {
    constructor() : this(0, UserModel(), "", "", "", Date(), null, ArrayList(), ArrayList())


}





