package com.interco.sotroll.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.interco.sotroll.database.entity.TopicsData
import com.interco.sotroll.utils.Listconverter

/**
 * Created by emine on 31/01/2019.
 */
@Database(entities = arrayOf(TopicsData::class), version = 1, exportSchema = true)
@TypeConverters(Listconverter::class)
abstract class TopicsDataBase : RoomDatabase() {

    abstract fun TopicsDataBase(): TopicsDataDao

    companion object {
        private var INSTANCE: TopicsDataBase? = null

        fun getInstance(context: Context): TopicsDataBase? {
            if (INSTANCE == null) {
                synchronized(TopicsDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TopicsDataBase::class.java, "event-soatDB.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}