package com.interco.sotroll.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.interco.sotroll.models.UserModel
import java.util.*

/**
 * Created by emine on 12/02/2019.
 */
@Entity(tableName = "topic_Comments")
data class CommentsData(
        @PrimaryKey(autoGenerate = true)
        var id: Long? = 0,
        var user: UserModel? = null,
        var content: String? = null,
        var createdDate: Date? = null

)


