package com.interco.sotroll.di.componant

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel

import com.interco.sotroll.NETWORK.NetworkApiService
import com.interco.sotroll.base.fragment.BaseFragment
import com.interco.sotroll.database.TopicsDataBase
import com.interco.sotroll.di.Injectables
import com.interco.sotroll.di.module.AppModule
import com.interco.sotroll.di.scope.AppScope
import com.interco.sotroll.models.UserModel

import dagger.Component
import javax.inject.Singleton

/**
 * Created by emine on 14/12/2018.
 */

@Component(modules = arrayOf(AppModule::class))
@AppScope
interface AppComponant {
    @AppScope
    fun provideApiService(): NetworkApiService

    @Singleton
    fun provideConext(): Context


    @Singleton
    fun provideDatabase(): TopicsDataBase

    @Singleton
    fun sharedPrefsManager(): SharedPreferences

    @Singleton
    fun inject(application: Application)

    @Singleton
    fun inject(injectables: Injectables)

    @Singleton
    fun inject(baseViewModel: AndroidViewModel)

   

}
