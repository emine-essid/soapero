package com.interco.sotroll.di.module

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.interco.sotroll.BuildConfig
import com.interco.sotroll.NETWORK.NetworkApiService
import com.interco.sotroll.database.TopicsDataBase
import com.interco.sotroll.di.scope.AppScope
import com.interco.sotroll.utils.ConnectivityInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by emine on 14/12/2018.
 */
@Module
class AppModule
// private String API_KEY = "iidsgunygg2kud3yt6ttcnd6ilt792";

(internal var context: Context) {

    @Provides
    @AppScope
    internal fun provideGson(): Gson {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        return gson
    }


    @Provides
    @AppScope
    internal fun provideConext(): Context {
        return context
    }

    @Provides
    @AppScope
    internal fun provideRetrofit(gson: Gson): Retrofit {

        val requestInterceptor = { chain: Interceptor.Chain ->


            val url = chain.request()
                    .url()
                    .newBuilder()
                    //.addQueryParameter("key", API_KEY)
                    .build()

            val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

            Log.e("**", "-----------> " + request.toString())

            chain.proceed(request)
        }


        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addNetworkInterceptor(StethoInterceptor())
                .addInterceptor(ConnectivityInterceptor(context))
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build()

        //https://api.barcodelookup.com/v2/   products?barcode=9780140157376  &key=iidsgunygg2kud3yt6ttcnd6ilt792
        val retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))

                .build()
        return retrofit
    }


    @Provides
    @AppScope
    internal fun provideApiService(gson: Gson, retrofit: Retrofit): NetworkApiService {
        val networkApiService = NetworkApiService(gson, retrofit)
        return networkApiService
    }


    @Provides
    @AppScope
    internal fun sharedPrefsManager(): SharedPreferences {
        return context.getSharedPreferences(BuildConfig.sharedPrefsName, Context.MODE_PRIVATE)
    }

    @Provides
    @AppScope
    internal fun provideDataBase(): TopicsDataBase {
        return TopicsDataBase.getInstance(context)!!
    }




}
