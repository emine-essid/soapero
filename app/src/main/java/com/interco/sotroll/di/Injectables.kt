package com.interco.sotroll.di

import com.interco.sotroll.di.componant.AppComponant
import com.interco.sotroll.root.App

/**
 * Created by emine on 20/01/2019.
 */
open class Injectables {
    internal var appComponant: AppComponant? = null

    init {
        try {
            appComponant = App.appComponant
            appComponant!!.inject(this)
        } catch (T: Exception) {
            //TEST IS RUNNING
        }

    }
}
