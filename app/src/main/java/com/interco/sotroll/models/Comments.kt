package com.interco.sotroll.models

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by emine on 12/02/2019.
 */
data class Comments(
        @SerializedName("id")
        var id: Long? = null,
        @SerializedName("user")
        var user: UserModel? = null,
        @SerializedName("content")
        var content: String? = null,
        @SerializedName("createdDate")
        var createdDate: Date? = null

/*
"id": 6,
        "content": "cjjfdukbvv emine",
        "createdDate": "2019-02-22T10:56:21.149+0000",
        "user": {
 */

)


