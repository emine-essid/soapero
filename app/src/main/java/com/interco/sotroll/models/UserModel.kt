package com.interco.sotroll.models

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by emine on 12/02/2019.
 */
@Entity(tableName = "UserModel")
data class UserModel(
        @PrimaryKey(autoGenerate = true)
        var id: Long? = 0,
        var nom: String? = null,
        var email: String? = null,
        var photo: String? = null,
        var role: Boolean = false

)
