package com.interco.sotroll.models

import com.interco.sotroll.database.entity.CommentsData
import java.util.*

/**
 * Created by emine on 12/02/2019.
 */
data class Topic(
        var id: Long? = null,
        var owner: UserModel? = null,
        var picture: String? = null,
        var content: String? = null,
        var location: String? = null,
        var createdDate: Date? = null,
        var dateRdv: Date? = null,
        var nbrLikes: MutableList<UserModel> = ArrayList(),
        var commentList: MutableList<Comments> = ArrayList()


) {
    constructor(creator: UserModel?, imageUrl: String, content: String?, location: String?, createdDate: Date?, date_RDV: Date?, likesList: MutableList<UserModel>, comments: MutableList<CommentsData>) : this()


}
