package com.interco.sotroll.root

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.interco.sotroll.di.componant.AppComponant
import com.interco.sotroll.di.componant.DaggerAppComponant
import com.interco.sotroll.di.module.AppModule
import io.fabric.sdk.android.Fabric
import timber.log.Timber



/**
 * Created by emine on 14/12/2018.
 */
class App : Application() {


    /** * Dear Programmer:
     * * * *   My name is Emine and when I wrote this code, only God and I knew how it worked.
     * * * *  Now, only God knows it!
     * * * *  Therefore, if you are trying to optimize this routine and
     * * * *  it fails (most surely), please increase this counter
     * * * *  as a warning for the next person:
     * * * *  total_hours_wasted_here: 1
     */


    override fun onCreate() {
        super.onCreate()

        appComponant = DaggerAppComponant.builder()
                .appModule(AppModule(applicationContext))
                .build()

        appComponant!!.inject(this)

        Fabric.with(this, Crashlytics())

        Stetho.initializeWithDefaults(this)

        Timber.plant(Timber.DebugTree())


    }

    companion object {
        var appComponant: AppComponant? = null
            private set
    }

}
